import { IsPositive, IsNotEmpty } from 'class-validator';
class CreatedOrderItemDto {
  @IsNotEmpty()
  productId: number;
  @IsPositive()
  @IsNotEmpty()
  amount: number;
}

export class CreateOrderDto {
  @IsNotEmpty()
  customerId: number;
  @IsNotEmpty()
  orderItem: CreatedOrderItemDto[];
}
